@extends('layouts.app') 

@section('title')
	{{$author->name}} <a class="btn btn-default" href="{{action('AuthorController@edit', $author->id)}}"><i class="fa fa-pencil"></i></a>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-2">
		{!! Form::open(['route' => ['author.destroy', $author->id], 'method'=>'DELETE']) !!}
		{!! Form::submit('Verwijder', array('class'=>'btn btn-danger')) !!}
		{!! Form::close() !!}
	</div>
	<br/>
	<div class="col-sm-6">
			{{$author->name}}<br/>
			<br/>
	</div>
</div>
@endsection
