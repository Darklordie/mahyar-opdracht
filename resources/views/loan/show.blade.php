@extends('layouts.app') 

@section('title')
	Uitleen-ID ({{$loan->id}}) <a class="btn btn-default" href="{{action('LoanController@edit', $loan->id)}}"><i class="fa fa-pencil"></i></a>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-2">
		{!! Form::open(['route' => ['loan.destroy', $loan->id], 'method'=>'DELETE']) !!}
		{!! Form::submit('Verwijder', array('class'=>'btn btn-danger')) !!}
		{!! Form::close() !!}
	</div>
	<br/>
	<div class="col-sm-6">
			<i class="fa fa-at"></i>&nbsp: {{$loan->id}}<br/>
			<br/>
	</div>
</div>
@endsection