@extends('layouts.app')

@section('title')
	Boek uitlenen
@endsection

@section('tools')
<li role="navigation">
	<a onClick="window.history.back()">
		<i class="fa fa-arrow-left"></i>&nbspTerug
	</a>
</li>
@endsection

@section('content')
{!! Form::open(['route' => ['loan.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="col-sm-4">
		{!! Form::label('startdate', 'Uitleendatum', ['class' => 'control-label']) !!}
		{!! Form::date('startdate', null, ['class' => 'form-control']) !!}
	</div>
	<div class="col-sm-4">
		{!! Form::label('expirydate', 'Verloopdatum', ['class' => 'control-label']) !!}
		{!! Form::date('expirydate', null, ['class' => 'form-control']) !!}
	</div>
    <div class="col-sm-4">
		{!! Form::label('returndate', 'Terugbrengdatum', ['class' => 'control-label']) !!}
		{!! Form::date('returndate', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-primary">
			<i class="fa fa-btn fa-sign-in"></i> Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}
@endsection
