@extends('layouts.app')

@section('title')
	Nieuwe gebruiker toevoegen
@endsection

@section('tools')
<li role="navigation">
	<a onClick="window.history.back()">
		<i class="fa fa-arrow-left"></i>&nbspTerug
	</a>
</li>
@endsection

@section('content')
{!! Form::open(['route' => ['user.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="col-sm-6">
		{!! Form::label('name', 'Naam*', ['class' => 'control-label']) !!}
		{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'De naam hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
		{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Het e-mailadres hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('role_id', 'Rol', ['class' => 'control-label']) !!}
		{!! Form::select('role_id', $roles, null, ['class' => 'form-control', 'placeholder' => 'Maak een keuze uit de lijst']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('postcode', 'Postcode*', ['class' => 'control-label']) !!}
		{!! Form::text('postcode', null, ['class' => 'form-control', 'placeholder' => 'De postcode hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('huisnummer', 'Huisnummer', ['class' => 'control-label']) !!}
		{!! Form::text('huisnummer', null, ['class' => 'form-control', 'placeholder' => 'Het huisnummer hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('straatnaam', 'Straatnaam*', ['class' => 'control-label']) !!}
		{!! Form::text('straatnaam', null, ['class' => 'form-control', 'placeholder' => 'De straatnaam hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('woonplaats', 'Woonplaats*', ['class' => 'control-label']) !!}
		{!! Form::text('woonplaats', null, ['class' => 'form-control', 'placeholder' => 'De straatnaam hier']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-primary">
			<i class="fa fa-btn fa-sign-in"></i> Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}
@endsection
@section('scripts')
<script type="text/javascript">
$("#postcode").on('change', function() {
var postcode = $(this).val();
if (postcode.length > 1 && postcode.length < 14) {
console.log(postcode);
postcodeZoeken(postcode);
}
});
function postcodeZoeken() {
  var postcode = document.getElementById('postcode').value;
  var number = document.getElementById('huisnummer').value;
  console.log(postcode);
  console.log(number);
  $.ajax({
    url:"https://postcode-api.apiwise.nl/v2/addresses/?postcode=" + postcode + "&number=" + number,
    dataType: "json",
    type: "GET",
    headers: {
      "X-Api-Key": "ZWJi9fKaRR2c1PsM6OIWc8oAIXaibtXtaqfttlWN"
    },
    success: function(data) {
      console.log(data);
      for (i=0; i<data._embedded.addresses.length; i++) {
        document.getElementById('straatnaam').value = data._embedded.addresses[i].street;
        document.getElementById('woonplaats').value = data._embedded.addresses[i].city.label;
      }
    }
  });
}

document.getElementById('button').addEventListener('click', postcodeZoeken, false);
</script>
@endsection
