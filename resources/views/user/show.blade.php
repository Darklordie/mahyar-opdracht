@extends('layouts.app')

@section('title')
	({{$user->id}}) {{$user->name}} <a class="btn btn-default" href="{{action('UserController@edit', $user->id)}}"><i class="fa fa-pencil"></i></a>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-2">
		{!! Form::open(['route' => ['user.destroy', $user->id], 'method'=>'DELETE']) !!}
		{!! Form::submit('Verwijder', array('class'=>'btn btn-danger')) !!}
		{!! Form::close() !!}
	</div>
	<br/>
	<div class="col-sm-6">
			{{$user->email}}<br/>
			<br/>
	</div>
</div>

@endsection
