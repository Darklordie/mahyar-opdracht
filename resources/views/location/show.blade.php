@extends('layouts.app')

@section('title')
	({{$location->id}}) {{$location->name}} <a class="btn btn-default" href="{{action('LocationController@edit', $location->id)}}"><i class="fa fa-pencil"></i></a>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-2">
		{!! Form::open(['route' => ['location.destroy', $location->id], 'method'=>'DELETE']) !!}
		{!! Form::submit('Verwijder', array('class'=>'btn btn-danger')) !!}
		{!! Form::close() !!}
	</div>
	<br/>

</div>
@endsection
