@extends('layouts.app')

@section('title')
	Kopie: {{$copy->id}} <br />
	Van titel: "{{$copy->book->title}}" {{$copy->name}} <a class="btn btn-default" href="{{action('CopyController@edit', $copy->id)}}"><i class="fa fa-pencil"></i></a>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-2">
			{!! Form::open(['route' => ['copy.destroy', $copy->id], 'method'=>'DELETE']) !!}
			{!! Form::submit('Verwijder', array('class'=>'btn btn-danger')) !!}
			{!! Form::close() !!}
		</div>
		<br/>
		<div class="col-xs-12">
			<div class="col-sm-4">
				Datum gekocht: &nbsp; {{$copy->datebought}}<br/>
				<br/>
			</div>
			<div class="col-sm-4">
				Staat: &nbsp; {{$copy->state}}<br/>
				<br/>
			</div>
			<div class="col-sm-4">
				Titel: &nbsp; {{$copy->book->title}}<br/>
				<br/>
			</div>
		</div>
	</div>
@endsection
