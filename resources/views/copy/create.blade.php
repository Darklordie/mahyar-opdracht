@extends('layouts.app')

@section('title')
    Nieuw exemplaar toevoegen
@endsection

@section('tools')
    <li role="navigation">
        <a onClick="window.history.back()">
            <i class="fa fa-arrow-left"></i>&nbspTerug
        </a>
    </li>
@endsection

@section('content')
    {!! Form::open(['route' => ['copy.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
        <div class="col-sm-6">
            {!! Form::label('datebought', 'Aanschafdatum', ['class' => 'control-label']) !!}
            {!! Form::date('datebought', null, ['class' => 'form-control', 'placeholder' => 'De naam hier']) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('state', 'Staat', ['class' => 'control-label']) !!}
            {!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'De staat van het boek hier']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('book_id', 'Titel', ['class' => 'control-label']) !!}
            {!! Form::select('book_id', $books, null, ['class' => 'form-control', 'placeholder' => 'Maak een keuze uit de lijst']) !!}
            {{--//hiermee kunnen we wel de placeholder disabled laten--}}
            {{--<select name="book_id" class="form-control">--}}
            {{--<option value="" disabled selected>Kies de bijbehorende titel</option>--}}
            {{--@foreach($books as $book)--}}
            {{--<option value="{!! $book->id !!}">{!! $book->title !!}</option>--}}
            {{--@endforeach--}}
            {{--</select>--}}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">

            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-sign-in"></i> Opslaan
            </button>
        </div>
    </div>
    {!! Form::close() !!}
@endsection