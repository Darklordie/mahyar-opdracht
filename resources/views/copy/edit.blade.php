@extends('layouts.app')

@section('title')
	Bewerk exemplaar
@endsection

@section('content')
	{!! Form::model($copy, ['route' => ['copy.update', $copy->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
	<div class="form-group">
		<div class="col-sm-6">
			{!! Form::label('datebought', 'Aanschafdatum', ['class' => 'control-label']) !!}
			{!! Form::date('datebought', null, ['class' => 'form-control', 'placeholder' => 'De naam hier']) !!}
		</div>
		<div class="col-sm-6">
			{!! Form::label('state', 'Staat', ['class' => 'control-label']) !!}
			{!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'De staat van het boek hier']) !!}
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-12">
			{!! Form::label('book_id', 'Titel', ['class' => 'control-label']) !!}
			{!! Form::select('book_id', $books, null, ['class' => 'form-control']) !!}

			{{--//hiermee is het wel mogelijk om de placeholder disabeld te laten.--}}
			{{--<select name="book_id" class="form-control">--}}
				{{--<option selected value="{!! $copy->book->title !!}">{!! $copy->book->title !!}</option>--}}
				{{--@foreach($books as $book)--}}
					{{--@if($book->id != $copy->book->id)--}}
						{{--<option value="{!! $book->id !!}">{!! $book->title !!}</option>--}}
					{{--@endif--}}
				{{--@endforeach--}}
			{{--</select>--}}
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary">
				<i class="fa fa-btn fa-sign-in"></i> Opslaan
			</button>
		</div>
	</div>


	{!! Form::close() !!}

@endsection
