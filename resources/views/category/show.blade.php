@extends('layouts.app')

@section('title')
	({{$category->id}}) {{$category->name}} <a class="btn btn-default" href="{{action('CategoryController@edit', $category->id)}}"><i class="fa fa-pencil"></i></a>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-2">
		{!! Form::open(['route' => ['category.destroy', $category->id], 'method'=>'DELETE']) !!}
		{!! Form::submit('Verwijder', array('class'=>'btn btn-danger')) !!}
		{!! Form::close() !!}
	</div>
	<br/>
	<div class="col-sm-6">
			<i class="fa fa-at"></i>&nbsp: {{$category->email}}<br/>
			<br/>
	</div>
</div>
@endsection
